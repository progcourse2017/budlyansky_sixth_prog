// FibonacciNumbers.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <ctime>


using namespace std;

int recFN(int value);
void forFN(int value);



int main()
{
	int valueOfNum;
	int startTime,
		endTime;


	cout << "Enter the value of 'Fibonacci numbers' :";
	cin >> valueOfNum;

	////////////////////////
	startTime = clock();
			forFN(valueOfNum);
	endTime = clock();

	cout << "\nTime: " << endTime - startTime;


	////////////////////////
	cout << "\n***************************\n";
	startTime = clock();
					cout << recFN(valueOfNum) <<' ';
    endTime = clock();

	cout << "\nTime: " << endTime - startTime << endl;
	///////////////////////


	system("pause");


    return 0;
}


void forFN(int value) 
{
	int *masF = new int[value];

	masF[0] = 0;
	masF[1] = 1;

	for(int i = 2; i <= value;i++)
	{
		masF[i] = masF[i - 2] + masF[i - 1];
	}

	cout << masF[value];
	
};

int recFN(int value)
{
	return value < 2 ? value : recFN(value - 2) + recFN(value - 1);
};